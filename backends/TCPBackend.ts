import {Backend, BackendConstructor} from "../lib/Backend";
import {executeString, Command, Context} from "../lib/Command";
import * as net from "net";

export class TCPBackend implements Backend {
    private server: net.Server;
    private socket: net.Socket;
    
    commands: Command[] = [];
    
    start() {
        this.server = net.createServer();
        this.init();
        console.log("TCPBackend started");
    }
    
    send(msg: string, ctx: Context): void {
        this.socket.write(`${msg}\n`);
    }
    
    private init() {
        this.server.on('connection', sock => {
            this.socket = sock;
            let buffer: string = '';
            this.socket.on('data', chunk => {
                let i = 0, piece = '', offset = 0;
        		buffer += chunk;
        		while ((i = buffer.indexOf('\n', offset)) !== -1) {
        			piece = buffer.substr(offset, i - offset);
        			offset = i + 1;
        			this.send(executeString(piece, this), null);
        		}
        		buffer = buffer.substr(offset);
            });
        });
        this.server.listen(3031, () => console.log('TCPBackend listening on port 3031'))
    }
    
    constructor() {}
};
