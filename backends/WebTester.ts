import {Backend, BackendConstructor} from "../lib/Backend";
import * as express from "express";
import {parse as parseURL} from "url";
import {executeString, Command, Context} from "../lib/Command";
import * as http from "http";
import * as socketio from "socket.io";

export class WebTester implements Backend {
    private app: express.Application;
    private io: any;
    private server: http.Server;
    
    commands: Command[] = [];
    start() {
        this.app = express();
        this.server = new http.Server(this.app);
        this.io = socketio(this.server);
        this.init();
        console.log("WebTester started");
    }
    
    send(msg: string, ctx: Context): void {
        this.io.emit('chat message', msg);
    }
    
    private init() {
        this.io.on('connection', (socket) => {
            socket.on('chat message', (msg) => {
                this.io.emit('chat message', msg);
                
                if (msg.startsWith('$') || msg.startsWith('#')) {
                    this.io.emit('chat message', executeString(msg, self));
                }
            });
        });
        
        this.app.get('/', function(req: express.Request, res: express.Response) {
            res.send(`
                <form action="/test" method="get">
                    Command: <textarea name="cmd"></textarea>
                    <button type="submit">Test</button>
                </form>
            `);
        });
        let self = this;
        this.app.get('/test', function(req: express.Request, res: express.Response) {
            res.send(`
<!doctype html>
<html>
  <head>
    <title>WebTester</title>
    <style>
      * { margin: 0; padding: 0; box-sizing: border-box; }
      body { font: 13px Helvetica, Arial; }
      form { background: #000; padding: 3px; position: fixed; bottom: 0; width: 100%; }
      form input { border: 0; padding: 10px; width: 90%; margin-right: .5%; }
      form button { width: 9%; background: rgb(130, 224, 255); border: none; padding: 10px; }
      #messages { list-style-type: none; margin: 0; padding: 0; }
      #messages li { padding: 5px 10px; }
      #messages li:nth-child(odd) { background: #eee; }
      #messages { margin-bottom: 40px }
    </style>
  </head>
  <body>
    <ul id="messages"><li>${executeString((req.query.cmd || '').toString(), self).replace(/&/g, '&amp;')
            .replace(/"/g, '&quot;')
            .replace(/</g, '&lt;')
            .replace(/>/g, '&gt;')}</li></ul>
    <form action="">
      <input id="m" autocomplete="off" /><button>Send</button>
    </form>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.4/socket.io.js"></script>
    <script src="https://code.jquery.com/jquery-1.11.1.js"></script>
    <script>
      $(function () {
        var socket = io();
        $('form').submit(function(){
          socket.emit('chat message', $('#m').val());
          $('#m').val('');
          return false;
        });
        socket.on('chat message', function(msg){
          $('#messages').append($('<li>').text(msg));
          window.scrollTo(0, document.body.scrollHeight);
        });
      });
    </script>
  </body>
</html>
            `);
        });
        this.server.listen(3030, () => console.log('WebTester listening on port 3030'))
    }
    constructor() {}
};
