import {Command, Context} from "./Command"

export interface Backend {
    start(): void;
    commands: Command[];
    send(msg: string, ctx: Context): void;
}

export interface BackendConstructor {
    new(): Backend;
}
