import {Arguments, Context, Command} from "../lib/Command";

class EchoCommand implements Command {
    constructor() {}
    
    execute(args: Arguments, ctx: Context) {
        ctx.reply(`echo: ${args.args}`);
        setTimeout(() => ctx.reply(`echo 2: ${args.args}`), 5000);
    }
    
    readonly isIdempotent: boolean = true;
    readonly aliases: string[] = ["echo"];
}

export default EchoCommand;