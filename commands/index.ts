import {Command, CommandConstructor} from '../lib/Command';

import EchoCommand from './EchoCommand';
import ZalgoCommand from './ZalgoCommand';
import RepeatingZalgoCommand from './RepeatingZalgoCommand';

let commands: Command[] = [EchoCommand, ZalgoCommand, RepeatingZalgoCommand].map(e => new e());

export default commands;