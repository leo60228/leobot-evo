import {Arguments, Context, Command} from "../lib/Command";
import {encode as bent} from 'lunicode-bent';
import {encode as creepify} from 'lunicode-creepify';
import * as randomCase from 'random-case';

class ZalgoCommand implements Command {
    constructor() {}
    
    execute(args: Arguments, ctx: Context) {
        let origWords = args.args.split(/([-!"#$%&'()*+,./:;<=>?@\[\]^_`{ | }~])/g);
        
        let words = [...origWords];
        
        if (args.args.length <= 5) {
            for (let i = 0; i < Math.floor(Math.random()*20) + 15; i++) {
                words = [...words, ...origWords];
            }
        }
        
        let zalgoWords = words.map(e => {
            let manipulated: string = '';
            
            if (Math.random() <= 0.9) {
                if (Math.random() <= 0.7) {
                    manipulated = creepify(e);
                } else {
                    manipulated = bent(e);
                }
            } else {
                manipulated = e;
            }
            
            if (Math.random() > 0.7) {
                return randomCase(manipulated);
            } else {
                return manipulated;
            }
        });
        
        ctx.reply(zalgoWords.join(''));
    }
    
    readonly isIdempotent: boolean = true;
    readonly aliases: string[] = [/*"zalgo"*/];
}

export default ZalgoCommand;